package server_handler;

public class Client implements Runnable {
    int i;

    Client() {
        i = (int) (Math.random() * 1000);
    }

    @Override
    public void run() {
        Manager.add(this);
        synchronized (Main.manager) {
            Main.manager.notify();
        }
    }

    @Override
    public String toString() {
        return "" + i;
    }
}
