package server_handler;


import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Manager extends Thread {
    static boolean flag=false;
    static ConcurrentLinkedDeque<Client> queue=new ConcurrentLinkedDeque();
    synchronized  void caller(){
        while (true) {
            try {
                while (!queue.isEmpty()) {
                Server.serve(queue.poll().toString());
            }
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    static void add(Client client){
        queue.add(client);
    }

    @Override
    public void run() {
        caller();
    }
}
