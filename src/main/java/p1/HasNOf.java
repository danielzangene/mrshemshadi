package p1;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class HasNOf {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.err.print("input: ");
        String input = scanner.nextLine();
        int n = scanner.nextInt();
        System.out.println("result: "+decoder(input,n));
    }
    static public Set<Character> decoder(String in, int n){
        HashMap<Character,Integer>output=new HashMap<>();
        char[]inputs=in.toCharArray();
        for (int i=0;i<inputs.length;i++){
            if(output.containsKey(inputs[i])){
                int val=output.get(inputs[i]);
                output.replace(inputs[i],val+1);
            }else{
                output.put(inputs[i],1);
            }
        }

        Map<Character, Integer> collect = output.entrySet().stream()
                .filter(map -> map.getValue() == n)
                .collect(Collectors.toMap(p -> p.getKey(), p -> p.getValue()));
        return collect.keySet();
    }
}
