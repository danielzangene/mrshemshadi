package p1;


import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class PrimeNumbers {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.err.println("num?");
        Long input = scanner.nextLong();
        System.out.println(primeNumbers(input).size());
    }
    static List<Long> primeNumbers(long l){
        List<Long>primes=new ArrayList<>();
        for (long i=1;i<l;i++){
            if (isPrime(i)){
                primes.add(i);
            }
        }
        return primes;
    }
    static boolean isPrime(long l){
        for (long i=2;i<Math.sqrt(l);i++){
            if (l%i==0){
                return false;
            }
        }
        return true;
    }
}
