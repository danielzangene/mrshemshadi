package p1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

public class NumberInWords {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.err.print("num?");
        int input = scanner.nextInt();
        System.out.println(convertor(input));
    }

    static HashMap<Integer, String> map1() {
        HashMap<Integer, String> map = new HashMap<>();
        map.put(0, "");
        map.put(1, "one");
        map.put(2, "two");
        map.put(3, "three");
        map.put(4, "four");
        map.put(5, "five");
        map.put(6, "six");
        map.put(7, "seven");
        map.put(8, "eight");
        map.put(9, "nine");
        return map;
    }

    static HashMap<Integer, String> map2() {
        HashMap<Integer, String> map = new HashMap<>();
        map.put(0, "");
        map.put(2, "twenty");
        map.put(3, "thirty");
        map.put(4, "forty");
        map.put(5, "fifty");
        map.put(6, "sixty");
        map.put(7, "seventy");
        map.put(8, "eighty");
        map.put(9, "ninety");
        return map;
    }

    static HashMap<Integer, String> map3() {
        HashMap<Integer, String> map = new HashMap<>();
        map.put(0, "");
        map.put(1, "one-hundred");
        map.put(2, "two-hundred");
        map.put(3, "three-hundred");
        map.put(4, "four-hundred");
        map.put(5, "five-hundred");
        map.put(6, "six-hundred");
        map.put(7, "seven-hundred");
        map.put(8, "eight-hundred");
        map.put(9, "nine-hundred");
        return map;
    }

    static HashMap<Integer, String> tenToTwenty() {
        HashMap<Integer, String> map = new HashMap<>();
        map.put(10, "ten");
        map.put(11, "eleven");
        map.put(12, "twelve");
        map.put(13, "thirteen");
        map.put(14, "fourteen");
        map.put(15, "fifteen");
        map.put(16, "sixteen");
        map.put(17, "seventeen");
        map.put(18, "eighteen");
        map.put(19, "nineteen");
        return map;
    }

    static List<HashMap<Integer, String>> convertedList() {
        List<HashMap<Integer, String>> list = new ArrayList<>();
        list.add(map1());
        list.add(map2());
        list.add(map3());
        list.add(tenToTwenty());
        return list;
    }

    static String convertor(int input) {
        List<HashMap<Integer,String>>list=convertedList();
        String output = "";
        for (int i = 0; input > 0; i++) {
            if (i == 0 && list.get(3).containsKey(input % 100)) {
                System.out.println("flag");
                output = list.get(3).get(input % 100);
                input = input / 100;
                i++;
            } else {
                if (!"".equals(output)) {
                    output = "-" + output;
                }
                output = list.get(i).get(input % 10) + output;
                input /= 10;
            }
        }
        return output;
    }

}
