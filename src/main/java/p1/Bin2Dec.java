package p1;

import java.util.HashMap;
import java.util.Scanner;

public class Bin2Dec {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.err.print("num?");
        int input = scanner.nextInt();
        System.out.println(convertor(input));
    }

    static String convertor(int input) {
        String output = "";
        for (int i = 0; input > 0; i++) {
            output+=input%2;
            input/=2;
        }
        int i=0;
        for (;i<output.length();i++){
            if (output.charAt(i)!='0'){
                break;
            }
        }
        return output.substring(i,output.length());
    }
}
