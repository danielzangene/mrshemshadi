package p1;

import java.util.Scanner;

public class Fibomax {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.err.println("num?");
        int input = scanner.nextInt();
        System.out.println("result: " + fibMax(input));
    }

    static public int fibMax(int in) {
        int a = 1;
        int b = 1;
        while (b < in) {
            int tmp = a + b;
            a = b;
            b = tmp;
        }
        return a;
    }
}
