package p1;

import java.util.HashMap;
import java.util.Scanner;

public class RepeatedNum {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.err.print("input: ");
        String input = scanner.nextLine();
        System.out.println("result: "+decoder(input));
    }
    static public HashMap<Character, Integer> decoder(String in){
        HashMap<Character,Integer>output=new HashMap<>();
        char[]inputs=in.toCharArray();
        for (int i=0;i<inputs.length;i++){
            if(output.containsKey(inputs[i])){
                int val=output.get(inputs[i]);
                output.replace(inputs[i],val+1);
            }else{
                output.put(inputs[i],1);
            }
        }
        return output;
    }
}
