package p1;

import java.util.Scanner;

public class POP {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.err.println("num?");
        String input = scanner.nextLine();
        System.out.println("result: "+pop(input));
    }
    static public String pop(String str){
        while(str.length()>1) {
            if (str.length() % 2 != 0)
                str += "1";
            String[] input = str.split("");
            str = "";
            for (int i = 0; i < input.length; i += 2) {
                int a = Integer.parseInt(input[i]);
                int b = Integer.parseInt(input[i + 1]);
                String s = String.valueOf(a * b);
                str += s;
            }
        }
        return str;
    }
}