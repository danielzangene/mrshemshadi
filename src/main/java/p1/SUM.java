package p1;

public class SUM {
    public static void main(String[] args) {
        System.out.println(bigDecimalSUM("9999", "1"));
    }

    static public String bigDecimalSUM(String first, String sec) {
        String output = "";
        if (first.length() < sec.length()) {
            int len = sec.length() - first.length();
            for (int i = 0; i < len; i++) {
                first = "0" + first;
            }
        } else if (first.length() > sec.length()) {
            int len = first.length() - sec.length();
            for (int i = 0; i < len; i++) {
                sec = "0" + sec;
            }
        }
        int t = 0;
        for (int i = first.length() - 1; i >= 0; i--) {
            int f = Integer.parseInt("" + first.charAt(i));
            int s = Integer.parseInt("" + sec.charAt(i));
            int sum = (f + s + t);
            t = sum / 10;
            output = "" + (sum % 10) + output;
        }
        if (t > 0) {
            output = "" + t + output;
        }
        System.out.println(first + "\n" + sec);
        return output;
    }

}
