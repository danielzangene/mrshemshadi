package p1;

import java.util.Scanner;

public class PythagorasNum {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.err.print("inputs: ");
        int a = scanner.nextInt();
        int b=scanner.nextInt();
        int c=scanner.nextInt();
        System.out.println("result: "+pythagorasChecker(a,b,c));
    }
    static public boolean pythagorasChecker(int a,int b,int c){
        return (a+b<c)?false:((b+c<a)?false:((a+c<b)?false:true));
    }
}
