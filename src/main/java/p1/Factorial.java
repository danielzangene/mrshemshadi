package p1;

import java.util.Scanner;

public class Factorial {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.err.println("num?");
        int input = scanner.nextInt();
        System.out.println(input + "!= " + fact(input));
    }

    static public int fact(int n) {
        if (n == 1) {
            return 1;
        } else {
            return n * (fact(n - 1));
        }
    }
}
