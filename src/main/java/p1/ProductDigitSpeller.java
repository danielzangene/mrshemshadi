package p1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

public class ProductDigitSpeller {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.err.print("num?");
        int input = scanner.nextInt();
        int inputp = scanner.nextInt();
        System.out.println(convertor(input*inputp));
    }

    static HashMap<Integer, String> map() {
        HashMap<Integer, String> map = new HashMap<>();
        map.put(0, "zero");
        map.put(1, "one");
        map.put(2, "two");
        map.put(3, "three");
        map.put(4, "four");
        map.put(5, "five");
        map.put(6, "six");
        map.put(7, "seven");
        map.put(8, "eight");
        map.put(9, "nine");
        return map;
    }

    static String convertor(int input) {
        HashMap<Integer,String>map=map();
        String output = "";
        for (int i = 0; input > 0; i++) {
            output=" "+map.get(input%10)+output;
            input/=10;
        }
        return output;
    }

}
