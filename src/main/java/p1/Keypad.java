package p1;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.HashMap;
import java.util.Scanner;

/**
 *
 * @author Daniel
 */
public class Keypad {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.err.println("incodeing (1)/decoding(0) ???");
            String input = scanner.nextLine();
            switch (input.charAt(0)) {
                case '1':
                    System.out.println("enter text to incoding");
                    input = scanner.nextLine();
                    System.out.println(incoder(input));
                case '0':
                    System.out.println("enter numbers to decoding");
                    input = scanner.nextLine();
                    System.out.println(decoder(input));
            }
        }
    }

    static String incoder(String input) {
        String output = "";
        HashMap<Character, String> incodes = incodeInputs();
        for (int i = 0; i < input.length(); i++) {
            output += incodes.get(input.charAt(i));
        }
        return output;
    }

    static String decoder(String input) {
        String output = "";
        HashMap<String, Character> decodes = decodeInputs();
        int pointer;
        for (int i = 0; i < input.length(); i++) {
            pointer = i;
            i++;
            while (i < input.length() && input.charAt(pointer) == input.charAt(i)) {
                i++;
            }
            output += String.valueOf(decodes.get(input.substring(pointer, i)));//convert to string
            i--;
        }
        return output;
    }

    static HashMap incodeInputs() {

        HashMap<Character, String> incode = new HashMap<>();
        incode.put('.', "1");
        incode.put('!', "11");
        incode.put('?', "111");
        incode.put('a', "2");
        incode.put('b', "22");
        incode.put('c', "222");
        incode.put('d', "3");
        incode.put('e', "33");
        incode.put('f', "333");
        incode.put('g', "4");
        incode.put('h', "44");
        incode.put('i', "444");
        incode.put('j', "5");
        incode.put('k', "55");
        incode.put('l', "555");
        incode.put('m', "6");
        incode.put('n', "66");
        incode.put('o', "666");
        incode.put('p', "7");
        incode.put('q', "77");
        incode.put('r', "777");
        incode.put('s', "8");
        incode.put('t', "88");
        incode.put('u', "888");
        incode.put('v', "9");
        incode.put('w', "99");
        incode.put('x', "999");
        incode.put('y', "0");
        incode.put('z', "00");
        incode.put(' ', "000");
        return incode;
    }

    static HashMap decodeInputs() {

        HashMap<String, Character> decode = new HashMap<>();
        decode.put("1", '.');
        decode.put("11", '!');
        decode.put("111", '?');
        decode.put("2", 'a');
        decode.put("22", 'b');
        decode.put("222", 'c');
        decode.put("3", 'd');
        decode.put("33", 'e');
        decode.put("333", 'f');
        decode.put("4", 'g');
        decode.put("44", 'h');
        decode.put("444", 'i');
        decode.put("5", 'j');
        decode.put("55", 'k');
        decode.put("555", 'l');
        decode.put("6", 'm');
        decode.put("66", 'n');
        decode.put("666", 'o');
        decode.put("7", 'p');
        decode.put("77", 'q');
        decode.put("777", 'r');
        decode.put("8", 's');
        decode.put("88", 't');
        decode.put("888", 'u');
        decode.put("9", 'v');
        decode.put("99", 'w');
        decode.put("999", 'x');
        decode.put("0", 'y');
        decode.put("00", 'z');
        decode.put("000", ' ');
        decode.put(" ", Character.MIN_VALUE);
        return decode;
    }

}
